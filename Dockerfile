FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/.sh"]

COPY .sh /usr/bin/.sh
COPY target/.jar /usr/share//.jar
